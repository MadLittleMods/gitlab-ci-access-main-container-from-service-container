Access main app container from service container

Related issues

 - https://gitlab.com/gitlab-org/gitlab-runner/issues/1042#note_61788095
 - https://gitlab.com/gitlab-org/gitlab-runner/issues/2127#note_35604258
 - https://gitlab.com/gitlab-org/gitlab-selenium-server/issues/1
 - https://gitlab.com/gitlab-org/gitlab-runner/issues/2302
