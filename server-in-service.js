const Promise = require('bluebird');
const request = Promise.promisify(require('request'));
const path = require('path');
const fs = require('fs-extra');
const outputFile = Promise.promisify(fs.outputFile);
const readFile = Promise.promisify(fs.readFile);
const express = require('express');
const logger = require('morgan');

const OUTSIDE_URL = 'http://gitlab-runner.local:5000/';

const HOSTS_TO_COPY_PATH = process.env.CI_PROJECT_DIR ? path.join(process.env.CI_PROJECT_DIR, './hosts') : null;
const HOSTS_PATH = '/etc/hosts';

const app = express();
app.use(logger('dev'));


app.get('/copy-hosts', function (req, res) {
  if(HOSTS_TO_COPY_PATH) {
    readFile(HOSTS_TO_COPY_PATH, 'utf8')
      .then((data) => {
        return outputFile(HOSTS_PATH, data);
      })
      .then(() => {
        res.status(200).send(`${HOSTS_TO_COPY_PATH} copied to ${HOSTS_PATH}`);
      })
      .catch((err) => {
        res.status(500).send(`Problem copying hosts file (${HOSTS_TO_COPY_PATH} copied to ${HOSTS_PATH}), ${err.message}\n${err.stack}`);
        return err;
      });
  }
  else {
    res.status(500).send(`$CI_PROJECT_DIR was not defined. Hosts were NOT copied`);
  }
});

app.get('*', function (req, res) {

  let hostsToCopyFile;
  let hostsFile;

  Promise.props({
    hostsToCopyFile: HOSTS_TO_COPY_PATH && readFile(HOSTS_TO_COPY_PATH, 'utf8')
      .catch((err) => {
        return err;
      }),
    hostsFile: readFile(HOSTS_PATH, 'utf8')
      .catch((err) => {
        return err;
      }),
  })
    .then((result) => {
      hostsToCopyFile = result.hostsToCopyFile;
      hostsFile = result.hostsFile;

      // Here is where we try to contact the the main app container
      return request({
        method: 'GET',
        uri: OUTSIDE_URL,
        timeout: 5000,
      })
    })
    .then((resFromOutside) => {
      res.status(200).send(`
        Server in service is up!
        Hosts file to copy (${HOSTS_TO_COPY_PATH}): ${hostsToCopyFile}
        Hosts file (${HOSTS_PATH}): ${hostsFile}
        Contacting outside main container: ${resFromOutside.statusCode} ${OUTSIDE_URL} ${resFromOutside.body}
      `);
    })
    .catch((err) => {
      res.status(500).send(`
        Server in service is up!
        Hosts file to copy (${HOSTS_TO_COPY_PATH}): ${hostsToCopyFile}
        Hosts file (${HOSTS_PATH}): ${hostsFile}
        But we failed to contact the outside main container ${err}
      `);
    })
});


// Log out any error that may have been lost
process.on('unhandledRejection', function(reason/*, promise*/) {
  console.log('unhandledRejection', reason, reason.stack);
});

const port = 5555;
const server = app.listen(port, function () {
  console.log(`Server in service is listening on port ${port}!`);
});

process.on('SIGINT', function() {
  console.log('Received SIGINT, gracefully shutting down');
  server.close();
  process.exit();
});


module.exports = server;
