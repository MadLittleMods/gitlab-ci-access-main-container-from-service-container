const express = require('express');
const logger = require('morgan');

const app = express();
app.use(logger('dev'));

app.get('*', function (req, res) {
  res.status(200).send(`Main server is up!`);
});



// Log out any error that may have been lost
process.on('unhandledRejection', function(reason/*, promise*/) {
  console.log('unhandledRejection', reason, reason.stack);
});

const port = 5000;
const server = app.listen(port, function () {
  console.log(`Main server is listening on port ${port}!`);
});

process.on('SIGINT', function() {
  console.log('Received SIGINT, gracefully shutting down');
  server.close();
  process.exit();
});


module.exports = server;
