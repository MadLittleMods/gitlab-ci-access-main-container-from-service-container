FROM node:boron

USER root

WORKDIR /app

ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 6.11.3

# Install nvm
RUN wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.4/install.sh | bash

# Install Node and npm
RUN . $NVM_DIR/nvm.sh \
  && nvm install $NODE_VERSION \
  && nvm alias default $NODE_VERSION \
  && nvm use default

# Add Node and npm to path so the commands are available
ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

# Confirm installation
RUN node -v
RUN npm -v

# Install app dependencies
COPY package.json ./
RUN npm install

# Bundle app source
COPY . .

EXPOSE 5555
# Used to signal from the main container to copy over the hosts file
EXPOSE 9248

CMD ["/bin/bash", "-c", "npm run start-server-in-service"]
